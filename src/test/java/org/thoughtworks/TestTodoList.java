package org.thoughtworks;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.thoughtworks.TodoList.todoList;
import static org.thoughtworks.TodoList.toggleItemStatus;

public class TestTodoList {
    List returnedList = new ArrayList();

    @Test
    public void testAddingOfItemsIntoTodoList() {
        TodoList item = new TodoList("wake up at 7'o clock");

        int sizeOfTodoListBeforeAdding = todoList.size();
        returnedList = TodoList.addAnItem(item);

        assertEquals(returnedList.size(), sizeOfTodoListBeforeAdding + 1);
    }

    @Test
    public void testRemovalOfItemsFromTodoList() {
        TodoList item = new TodoList("Go for a walk");
        TodoList itemToBeRemoved = new TodoList("Go for a walk");

        returnedList = TodoList.addAnItem(item);
        int sizeOfTodoListBeforeRemoving = todoList.size();
        returnedList = TodoList.removeAnItem(itemToBeRemoved);

        assertEquals(returnedList.size(), sizeOfTodoListBeforeRemoving - 1);
    }

    @Test
    public void testRetrievalOfRemovedItemsFromTodoList() {
        TodoList item = new TodoList("Watch Anime");
        TodoList itemToBeRemoved = new TodoList("Watch Anime");

        returnedList = TodoList.addAnItem(item);
        returnedList = TodoList.removeAnItem(itemToBeRemoved);
        int sizeOfTodoListBeforeRetrieval = returnedList.size();
        returnedList = TodoList.retrieveAnItem(item);

        assertEquals(returnedList.size(), sizeOfTodoListBeforeRetrieval + 1);
    }

    @Test
    public void testStatusOfANoteInTodoList() {
        TodoList item = new TodoList("practice coding");
        String statusOfItemBeforeToggle = item.status;

        returnedList = toggleItemStatus(item);

        assertNotEquals(item.status, statusOfItemBeforeToggle);
    }
}
