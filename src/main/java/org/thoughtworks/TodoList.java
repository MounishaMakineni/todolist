package org.thoughtworks;

import java.util.*;
public class TodoList {
    static List<String> todoList = new ArrayList<>();
    static List<String> removedItemList = new ArrayList<>();
    String note;
    String status;
    TodoList(String note){
        this.note=note;
        status="not completed";
    }

    public static List<String> addAnItem(TodoList item){
        todoList.add(item.note);
        return todoList;
    }

    public static List<String> removeAnItem(TodoList item){
        todoList.remove(item.note);
        removedItemList.add(item.note);
        return todoList;
    }

    public static List<String> retrieveAnItem(TodoList item){
        removedItemList.remove(item.note);
        todoList.add(item.note);
        return todoList;
    }

    public static List<String> toggleItemStatus(TodoList item){
        item.status="completed";
        return todoList;
    }
}
